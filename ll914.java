package ll.java.read;

import java.io.File;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.util.Date;

public class ll912 {
	final static int SIZE_B = 1024;
	final static int SIZE_KB = SIZE_B * SIZE_B;
	final static int SIZE_MB = SIZE_KB * SIZE_B;
	final static int SIZE_GB = SIZE_MB * SIZE_B;
	public static long  GetFileSize(String p) {
		File hh = new File(p);
		long length = 0;
		if (hh.isDirectory()) {
			File[] flist = hh.listFiles();
			for (int i = 0; i < flist.length; i++) {
				if (flist[i].isDirectory()) {
					length+=GetFileSize(flist[i].getPath());
				} else {
					length += flist[i].length();
				}
			}	
		} else {
			File tt= new File(p);
			length = tt.length();
		}
		return length;
	}
	public static void FormetFileSize(long aa) {
	String size = "";
	DecimalFormat df = new DecimalFormat("#.000");
	if (aa < SIZE_B) {
		size = df.format((double) aa) + "B";
	} else if (aa < SIZE_KB) {
		size = df.format((double) aa / SIZE_B) + "KB";
	} else if (aa < SIZE_MB) {
		size = df.format((double) aa / SIZE_KB) + "MB";
	} else {
		size = df.format((double) aa / SIZE_MB) + "GB";
	}
	System.out.println("File size = "+size);
}
	public static void GetFileTime(String file) {
		File kk = new File(file);
		long time = kk.lastModified();
		String myString = DateFormat.getDateInstance().format(new Date(time));
		System.out.println("修改时间： " + myString);
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		if (args.length != 0) {
			long startTime = System.currentTimeMillis();
			String path = new String(args[0]);
			System.out.println("input: " + path);
			File ll = new File(path);
			if (ll.exists()) {
				System.out.println("File Name : " + ll.getName());
				long size = ll912.GetFileSize(path);
				ll912.FormetFileSize(size);
				ll912.GetFileTime(path);
				long endTime = System.currentTimeMillis();
				System.out.println("总共发费时间 : " +(endTime-startTime) +"毫秒");
			} else {
				System.out.println("输入路径错误，重新输入");
			}
		} else {
			System.out.println("input parements error : ");
		}
	}
}