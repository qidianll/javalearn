package ll.java.interfaces;


// Class cls
// Interface I
// Abstract A 

public interface MyInterface {
	void doWork();
//	public void doWork();
//	public abstract void doWork();
}


abstract class AClass  implements MyInterface{
	// abstract void doWork();
	@Override
	public void doWork() {
	}
}












class Cls2Class extends AClass{
	@Override
	public void doWork() {
	}
	
}


class ClsClass implements MyInterface{
	@Override
	public void doWork() {		
	}
}

