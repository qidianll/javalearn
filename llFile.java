package ll.java.read;
import java.io.File;
import java.text.DecimalFormat;
import java.util.Calendar;
public class ll911 {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//判断文件大小是否为0
		if(args.length != 0) {
				//创建字符串对象path
				String path = new String(args[0]);
				//获取对象的所对应的路径名
                System.out.println("input: "+path);
                //创建File对象ll
                File ll = new File(path);
                //获取文件名
                System.out.println("File Name : " + ll.getName());
                //创建字符串变量size
                String size = "";
                //获取ll对象的大小
                long aa = ll.length();
                //DecimalFormat是NumberFormat的一个具体子类，用于格式化十进制数字，括号里面的'#'代表一个数字,不包括0，
                //'0'也代表一个数字，包括0。
                DecimalFormat df = new DecimalFormat("#.00"); 
                if (aa < 1024) {
                         size = df.format((double) aa) + "BT";
                         System.out.println(size);
                } else if (aa < 1048576) {
                         size = df.format((double) aa / 1024) + "KB";
                         System.out.println(size);
                } else if (aa < 1073741824) {
                         size = df.format((double) aa / 1048576) + "MB";
                         System.out.println(size);
                } else {
                         size = df.format((double) aa / 1073741824) +"GB";
                         System.out.println(size);
                }
                //调用抽象类Calendar(日历)里面的getInstance函数
                Calendar kk = Calendar.getInstance();
              //获取文件最后修改时间
                long bb = ll.lastModified();
				kk.setTimeInMillis(bb);
                System.out.println("修改时间： " + kk.getTime().toLocaleString());
                }else {
                	 	System.out.println("input parements error : ");
                		}
			}
}